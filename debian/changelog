grafx2 (2.8+ds-4) unstable; urgency=medium

  * Bump standards version to 4.7.0.
  * d/control: update build-depends pkg-config to pkgconf,
    libfontconfig1-dev to libfontconfig-dev.

 -- Alex Myczko <tar@debian.org>  Mon, 22 Jul 2024 12:58:55 +0000

grafx2 (2.8+ds-3) unstable; urgency=medium

  * Apply upstream patch to fix text tool. (Closes: #1009703)
  * Update maintainer address.
  * Bump standards version to 4.6.1.

 -- Gürkan Myczko <tar@debian.org>  Fri, 07 Oct 2022 13:48:48 +0200

grafx2 (2.8+ds-2) unstable; urgency=medium

  * Upload to unstable

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Sun, 15 Aug 2021 11:49:53 +0200

grafx2 (2.8+ds-1) experimental; urgency=medium

  * New upstream version. (Closes: #985544)
  * d/copyright: update years, add Upstream-Contact.
  * Bump standards version to 4.5.1.
  * Bump debhelper version to 13, drop d/compat.
  * d/upstream/metadata: added.
  * d/control: added Rules-Requires-Root.
  * d/watch: drop template part.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Thu, 25 Mar 2021 10:58:12 +0100

grafx2 (2.7-1) unstable; urgency=medium

  * New upstream version.
  * Add Vcs fields.
  * Bump standards version to 4.5.0.
  * debian/patches: dropped.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Sat, 01 Feb 2020 13:19:22 +0100

grafx2 (2.6+git20191023-1) experimental; urgency=medium

  * New upstream version.
  * Bump standards version to 4.4.1.
  * d/copyright: update to machine readable format.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Wed, 23 Oct 2019 07:35:38 +0200

grafx2 (2.6-2) unstable; urgency=medium

  * Patch src/fileformats to fix png saving.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Mon, 21 Jan 2019 13:44:37 +0100

grafx2 (2.6-1) unstable; urgency=medium

  * New upstream version. (Closes: #918725)
  * debian/control: add libtiff-dev.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Thu, 10 Jan 2019 16:51:38 +0100

grafx2 (2.5+git20181211-1) unstable; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Tue, 11 Dec 2018 09:52:29 +0100

grafx2 (2.5+git20181210-1) unstable; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Sat, 08 Dec 2018 13:34:43 +0100

grafx2 (2.5+git20181014-2) unstable; urgency=medium

  * Update liblua dependency to 5.3.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Thu, 06 Dec 2018 13:48:04 +0100

grafx2 (2.5+git20181014-1) unstable; urgency=medium

  * New upstream version.
  * debian/control: switch to SDL2.
  * Bump standards version to 4.2.1.
  * debian/watch: updated.
  * debian/README.source: added.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Tue, 09 Oct 2018 11:19:51 +0200

grafx2 (2.4+git20180105-2) unstable; urgency=medium

  * misc/unix/grafx2.desktop: updated.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Tue, 04 Sep 2018 16:17:36 +0200

grafx2 (2.4+git20180105-1) unstable; urgency=medium

  * New upstream version. (Closes: #702657)
  * Upstream fix for "Parameter missing...". (Closes: #748333)
  * Acknowledge NMU. Thanks to gregor herrmann. (Closes: #669433)
  * Bump standards version to 4.1.3.
  * Bump debhelper version to 11.
  * Change my name.
  * Drop debian/menu.
  * debian/rules: modernized.
  * debian/watch: update URL.
  * debian/control: add missing b-d libfontconfig1-dev.
  * debian/copyright: update URL and years.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Fri, 05 Jan 2018 13:10:25 +0100

grafx2 (2.3-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix "FTBFS: fileformats.c:30:17: fatal error: png.h: No such file or
    directory": add build dependency on libpng-dev.
    (Closes: #669433)

 -- gregor herrmann <gregoa@debian.org>  Fri, 15 Jun 2012 15:34:23 +0200

grafx2 (2.3-1) unstable; urgency=low

  * New upstream version.
  * Bump standards version to 3.9.2.
  * Drop patch to fix strings, applied upstream.
  * Bump debhelper version to 8.

 -- Gürkan Sengün <gurkan@phys.ethz.ch>  Thu, 14 Apr 2011 13:38:19 +0200

grafx2 (2.2-1) unstable; urgency=low

  * New upstream version, fixes FTBFS with binutils-gold. (Closes: #554742)
  * Bump standards version to 3.8.4.
  * debian/control: Add liblua5.1-0-dev and pkg-config to build depends.
  * debian/rules: Drop dh_desktop call.
  * debian/copyright: Update years.
  * Switch to dpkg-source format version 3.0 (quilt).
  * debian/watch: Added.
  * Added patch to fix spelling errors in source code.

 -- Gürkan Sengün <gurkan@phys.ethz.ch>  Mon, 22 Mar 2010 12:07:47 +0100

grafx2 (2.1-1) unstable; urgency=low

  * New upstream version.

 -- Gürkan Sengün <gurkan@phys.ethz.ch>  Mon, 21 Sep 2009 14:24:19 +0200

grafx2 (2.0-1) unstable; urgency=low

  * New upstream version.
  * Drop manual page since it comes with upstream now.

 -- Gürkan Sengün <gurkan@phys.ethz.ch>  Wed, 10 Jun 2009 10:44:35 +0200

grafx2 (2+20090319-1) unstable; urgency=low

  * Initial release. (Closes: #516724)

 -- Gürkan Sengün <gurkan@phys.ethz.ch>  Thu, 19 Mar 2009 12:15:06 +0100
